const kubeSmktest = require("../../index");
const ora = require("ora");

test("The pods are Active", async () => {
  // Define namespace:
  let namespace = "edutelling-smktest";

  let { passTest, podsStatus, report } = await kubeSmktest.statusPods(
    namespace
  );

  let logsSpinner = ora(
    `* Service Up: Pods status Namespace: ${namespace}`
  ).start();

  // Test decorators:
  if (!passTest) {
    logsSpinner.fail();
    console.log(report.render());
  } else {
    logsSpinner.succeed();
  }

  expect(passTest).toBe(true);
});
