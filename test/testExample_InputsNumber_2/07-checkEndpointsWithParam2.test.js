const ora = require("ora");
const config = require("../../src/config");
const swaggerSmktest = require("swagger-smktest");

test("Check Apis Endpoint only with parameters (2)", async () => {
  let urlSwagger = config.endpointSwagger;

  //! Its possible use /swagger.json
  let smktestCriterial = "basicWithAuth";

  let options = {
    tokenConfig: {
      curlRequest: config.curlTokenSwagger,
      tokenVariableName: "token",
      headers: {
        "Content-Type": "application/json",
        "accept-language": "en",
        Authorization: "Bearer TOKEN",
      },
    },
  };

  let {
    responseOfRequest,
    coverage,
    successSmokeTest,
    report,
    abstractReport,
  } = await swaggerSmktest.smokeTest(smktestCriterial, urlSwagger, options);

  //! Pint reports fails:
  if (!successSmokeTest) {
    console.log(report.render());
    console.log(abstractReport.render());
  }

  expect(successSmokeTest).toBe(false); //TODO false only for test
});
