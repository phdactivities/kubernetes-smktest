const kubeSmktest = require("../../index");
const ora = require("ora");

test("Create temporary Namespace ", async () => {
  //!
  let passTest = await kubeSmktest.createNamespaceTest();

  let logsSpinner = ora(
    `* Create Namespace: Create namespace enviroment: passTest=${passTest}`
  ).start();

  if (!passTest) {
    logsSpinner.fail();
  } else {
    logsSpinner.succeed();
  }

  expect(passTest).toBe(true);
});
