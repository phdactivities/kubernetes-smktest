const ora = require("ora");
const config = require("../../src/config");
const swaggerSmktest = require("swagger-smktest");

test("Check Apis Endpoint only with swaggerURL parameter (1)", async () => {
  //! Swagger documentation:
  let urlSwagger = config.endpointSwagger;
  //! Its possible use /swagger.json
  let smktestCriterial = "basic";

  let {
    responseOfRequest,
    coverage,
    successSmokeTest,
    report,
    abstractReport,
  } = await swaggerSmktest.smokeTest(smktestCriterial, urlSwagger);

  //! Pint reports fails:
  if (!successSmokeTest) {
    console.log(report.render());
    console.log(abstractReport.render());
  }

  expect(successSmokeTest).toBe(false); //TODO false only for test
});
