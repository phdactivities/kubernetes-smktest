const kubeSmktest = require("../../index");
const ora = require("ora");

test("Check logs service contents", async () => {
  let namespace = "edutelling-smktest";
  let { passTest, podsStatus, report } = await kubeSmktest.logsState(namespace);

  console.log(podsStatus);
  let logsSpinner = ora(
    `* Logs Check: Kubernetes LogsPods Namespace: ${namespace}`
  ).start();

  console.log(report.render());
  if (!passTest) {
    logsSpinner.fail();
  } else {
    logsSpinner.succeed();
  }

  expect(passTest).toBe(true);
});
