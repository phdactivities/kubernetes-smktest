const kubeSmktest = require("../../index");
const ora = require("ora");

test("IPs are aLive:", async () => {
  //! Select namespaces:
  let namespace = "edutelling-smktest";

  let { passTest, statusPingList, report } = await kubeSmktest.ping(namespace);

  // Information about the test status:
  let logsSpinner = ora(
    `* IP Adress is Alive: Namespace: ${namespace}`
  ).start();

  if (!passTest) {
    logsSpinner.fail();
    //! Print table report
    console.log(report.render());
  } else {
    logsSpinner.succeed();
  }
  expect(passTest).toBe(true);
}, 6000);
