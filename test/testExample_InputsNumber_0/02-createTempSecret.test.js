const kubeSmktest = require("../../index");
const ora = require("ora");

test("Create temporary secret inside of the cluster", async () => {
  //!
  let passTest = await kubeSmktest.createSecretTest();

  let logsSpinner = ora(
    `* Create Secret: Create namespace and secret: passTest=${passTest}`
  ).start();

  if (!passTest) {
    logsSpinner.fail();
  } else {
    logsSpinner.succeed();
  }

  expect(passTest).toBe(true);
});
