/* eslint-disable no-unused-vars */
const { capitalize, merge } = require("lodash");
const os = require("os");
const path = require("path");

global.Promise = require("bluebird");
global._ = require("lodash");

/* istanbul ignore next */
const requireProcessEnv = (NODE_ENV) => {
  if (!process.env[NODE_ENV]) {
    throw new Error("You must set the " + NODE_ENV + " environment variable");
  }
  return process.env[NODE_ENV];
};

/* istanbul ignore next */
if (process.env.NODE_ENV !== "production") {
  const dotenv = require("dotenv-safe");
  dotenv.config({
    path: path.join(__dirname, "../.env"),
    example: path.join(__dirname, "../.env.example"),
  });
}

const APP_NAME = requireProcessEnv("APP_NAME");

console.log(process.env.ENDPOINT_SWAGGER);
const config = {
  all: {
    endpointSwagger: process.env.ENDPOINT_SWAGGER,
    curlTokenSwagger: process.env.CURL_ENDPOINT_SWAGGER_TOKEN,
  },
  test: {},
  development: {},
  staging: {},
  production: {},
};

module.exports = merge(config.all, config[config.all.env]);
