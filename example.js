const k8s = require("@kubernetes/client-node");

async function createSecret(k8sApi) {
  var namespace = {
    metadata: {
      name: "smoke-test-temp-env",
      // namespace: "smoke-test-temp-env",
    },
  };

  let statusCode;

  return k8sApi.createNamespace(namespace).then(
    function (response) {
      console.log("Created namespace");
      // console.log(response);
      return k8sApi
        .readNamespace(namespace.metadata.name)
        .then(async (response) => {
          //? (1): Create namespace.
          let secretData = await k8sApi
            .createNamespacedSecret(namespace.metadata.name, {
              apiVersion: "v1",
              kind: "Secret",
              name: "secret-smoke-test-temp",
              metadata: { name: "secret-smoke-test-temp" },
            })
            .catch((e) => console.log(e));

          //? (2): Create example.
          k8sApi.deleteNamespace(
            namespace.metadata.name,
            {} /* delete options */
          );
          console.log(" 1)> ", response.response.statusCode);
          return secretData.response.statusCode;
        });
    },
    (err) => {
      console.log("Error!: " + err);
      return 402;
    }
  );
}

async function createSecretTest() {
  const kc = new k8s.KubeConfig();
  kc.loadFromDefault();

  const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
  let statusCode = await Promise.resolve(createSecret(k8sApi));

  let passTest;
  if (statusCode === 200) {
    passTest = true;
  }

  return passTest;
}

createSecretTest();
