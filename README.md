# kubernetes-smktest:

This is a library for creating smoke tests on kubernetes-based clusters. It is developed to have a high degree of automation although there are levels that require user configuration.

Smoke tests: It is a type of stability test that must be applied before starting the rest of the deep tests. It is generally maintenance-free and can help detect stability issues in your architecture early.

Keyworld: Smoke Test, Sporadic failures, automatic, test, kubernetes

# Content:

- [Smoke Test Commands](#markdown-header-span-elements)
- [General Commands](#markdown-header-span-elements)

| Commands            | Parameters                              | Test Type   | Description                                                        |
| :------------------ | :-------------------------------------- | :---------- | :----------------------------------------------------------------- |
| createNamespaceTest | not required                            | For Cluster | Verify if the cluster has the ability to create new ones NewSpaces |
| createSecretTest    | not required                            | For Cluster | Verify if the cluster has the ability to create new ones Secrets   |
| statusPods          | namespace                               | For Pods    | Verify that the cluster services are available.                    |
| ping                | namespace                               | For Pods    | Verify that network connections are available.                     |
| logsState           | namespace                               | For Pods    | Verify that there are no errors within the logs of each Pods.      |
| smokeTest           | namespace, smktestCriterial, urlSwagger | For Pod     | Check backend stability.                                           |

# 1) Use Example Smoke Test Commands:

## SmokeTest with Jest

### Test create new namespace:

      const kubeSmktest = require("swagger-smktest");
      const ora = require("ora");

      test("Create temporary Namespace ", async () => {
            //!
            let passTest = await kubeSmktest.createNamespaceTest();

            let logsSpinner = ora(
                `* Create Namespace: Create namespace enviroment: passTest=${passTest}`
            ).start();

            if (!passTest) {
              logsSpinner.fail();
            } else {
             logsSpinner.succeed();
            }

            expect(passTest).toBe(true);

      });

, check the create kubernetes secret:

### Test create new secret:

      const kubeSmktest = require("swagger-smktest");
      const ora = require("ora");

      test("Create temporary secret inside of the cluster", async () => {

            let passTest = await kubeSmktest.createSecretTest();

            let logsSpinner = ora(
              `* Create Secret: Create namespace and secret: passTest=${passTest}`
            ).start();

            if (!passTest) {
             logsSpinner.fail();
            } else {
             logsSpinner.succeed();
            }

            expect(passTest).toBe(true);

      });

### Service Up:

      const kubeSmktest = require("swagger-smktest");
      const ora = require("ora");

      test("The pods are Active", async () => {
            // Define namespace:
            let namespace = "test-smktest";

            let { passTest, podsStatus, report } = await kubeSmktest.statusPods(
                  namespace
            );

            let logsSpinner = ora(
             `* Service Up: Pods status Namespace: ${namespace}`
            ).start();

            // Test decorators:
            if (!passTest) {
               logsSpinner.fail();
               console.log(report.render());
            } else {
               logsSpinner.succeed();
            }

            expect(passTest).toBe(true);
      });

### PingTCP perform all internal services:

      const kubeSmktest = require("swagger-smktest");
      const ora = require("ora");

      test("IPs are aLive:", async () => {

            //! Select namespaces:
            let namespace = "test-smktest";

            let { passTest, statusPingList, report } = await kubeSmktest.ping(namespace);

            // Information about the test status:
            let logsSpinner = ora(
              `* IP Adress is Alive: Namespace: ${namespace}`
            ).start();

            if (!passTest) {
              logsSpinner.fail();
              //! Print table report
              console.log(report.render());
            } else {
              logsSpinner.succeed();
            }

      expect(passTest).toBe(true);
      }, 6000);

### Check if there are errors in the Logs within the Logs:

      const kubeSmktest = require("swagger-smktest");
      const ora = require("ora");

      test("Check logs service contents", async () => {
      let namespace = "smktest";
      let { passTest, podsStatus, report } = await kubeSmktest.logsState(namespace);


      let logsSpinner = ora(
        `* Logs Check: Kubernetes LogsPods Namespace: ${namespace}`
      ).start();


      if (!passTest) {
        logsSpinner.fail();
      } else {
       logsSpinner.succeed();
      }

      expect(passTest).toBe(true);
      });

### Check Apis Endpoint only with swaggerURL parameter (1):

      test("Check Apis Endpoint only with swaggerURL", async () => {

        //! Swagger documentation:
        let urlSwagger = 'swaggerURL';

        //! Its possible use /swagger.json
        let smktestCriterial = "basic";

        let {
            responseOfRequest,
            coverage,
            successSmokeTest,
            report,
            abstractReport,
            } = await swaggerSmktest.smokeTest(smktestCriterial, urlSwagger);

       //! Pint reports fails:
      if (!successSmokeTest) {
        console.log(report.render());
        console.log(abstractReport.render());
      }

      expect(successSmokeTest).toBe(false); //TODO false only for test
      });

# General Commands

| Command library | Description |
| :-------------- | :---------- |
| ...             | ....        |

### Output console:

      example code fragment

IMAGE EXAMPLE
![toolss_200px](/src/documentation/swagger-smktest.png)

npm run test --inputs=0
